# DevSpace company profile page
* Задача зверстати сторінку компанії

##### Сторінка профіль компанії має:
  * назву компанії, прикладу ExampleCompanyName
  * опис компанії
  * список країн де компанія має офіс у форматі emoji 🇺🇦🇺🇦🇺🇦
  * тип компанії: продуктова чи аутсорс
  * інформацію про компанію яка є на DOU.ua (завжди буде)
    * посилання на сторінку DOU профілю ~ "#" https://jobs.dou.ua/companies/example-com/
    * logo компанії 327x145, краще вставити червону картинку такого розміру
    * число відгуків та посилання "#" (або надпис "без відгуків" чи якось інакше позначити що відгуки відсутні)
    * число фотографій та посилання "#" (або надпис "без фотографій" чи якось інакше позначити що фотографії відсутні)
    * число співробітників ~ "понад 1500 спеціалістів"
    * число вакансій та посилання "#"
    * додаткова опціональна інформація
      * опціонально місце в рейтингу і посилання на рейтинг https://jobs.dou.ua/ratings/
      * опціонально значення "Я готовий(-а) рекомендувати компанію, де зараз працюю, своїм друзям та знайомим" від 0 до 100% та посилання на анкету https://jobs.dou.ua/companies/example-com/poll/ 
  * посилання на профіль LinkedIn та значок LinkedIn
  * також треба додати блок де будуть лого сервісу на посилання на профіль компанії в ньому
    * Dev.to
    * GitHub.com
    * GitLab.com
  * інформацію про компанію яка є на Glassdoor.com (може бути відсутній профіль, треба врахувати і написати що відсутні дані)
    * посилання на сторінку Glassdoor профілю
    * число відгуків та посилання "#" (або надпис "без відгуків" чи якось інакше позначити що відгуки відсутні)
    * число фотографій та посилання "#" (або надпис "без фотографій" чи якось інакше позначити що фотографії відсутні)
    * число вакансій та посилання "#"
    * оцінка компанії від 0 до 5 (зірочками)
  * інформацію про компанію яка є на Indeed.com (може бути відсутній профіль, треба врахувати і написати що відсутні дані)
    * посилання на сторінку Indeed профілю
    * число відгуків (або надпис "без відгуків" чи якось інакше позначити що відгуки відсутні)
    * число фотографій (або надпис "без фотографій" чи якось інакше позначити що фотографії відсутні)
    * число вакансій та посилання "#"
    * оцінка компанії від 0 до 5 (зірочками)
  * інформація про компанію на YouControl.com.ua (може бути відсутній профіль, треба врахувати і написати що відсутні дані)
    * посилання на сторінку YouControl профілю ~ "#"
  * інформація про компанію на Opendatabot.ua (може бути відсутній профіль, треба врахувати і написати що відсутні дані)
    * посилання на сторінку Opendatabot профілю ~ "#"
  * список публікації про компанію, публікація складається з:
    * назви публікації
    * дати публікації
    * посилання на публікацію
    * якщо публікація була на DOU, AIN, LinkedIn то також додати маленький значок цих сервісів
  * також треба блок для відображення локації
    * списку офісів та адреси
    * блоку з Google Maps на який виведу офіси

##### Сторінка списку компаній
  * Має зверху поле вводу для пошуку компанії по назві
  * Список всіх назв компаній без пагінації
    * Logo 327x145
    * Назва та посиланням на сторінку компанії "#"
    * список країн де компанія має офіс у форматі emoji 🇺🇦🇺🇦🇺🇦

##### Додатково:
  * Мета зібрати спільну інформацію про компанії в одному місці тому за бажанням можеш додати якісь свої критерії
  * Інформацію можеш структурувати як тобі самому буде комфортно